﻿namespace Storage
{
    
    class Program
    {
        static void Main(string[] args)
        {
            
            Model m = new Model();
            View v = new View(m);
            Controller c = new Controller(v, m);

            while (true) {
                c.main_menu();
            }
        }
    }
}
