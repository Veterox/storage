﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Storage
{
    class Controller
    {
        private delegate void AccountHandler(string message);
        private event AccountHandler Notify;
        private View v;
        private Model m;

        public Controller(View v, Model m) {
            Notify += display_message;
            this.v = v;
            this.m = m;
        }
        private static void display_message(string str){
            Console.WriteLine(str);
            Console.ReadLine();
        }
        private string get_options<T>(List<(string option, T action)> options_list)
        {
            string s = "";

            for (int i = 0; i < options_list.Count; i++) {
                s += (i + 1)+ ") " + options_list[i].option + "\n";
            }
            return s;
        }
        public void main_menu() {
            List<(string option, Action action)> options_list = new List<(string option, Action action)>() {
                ("Посмотреть базу данных о продуктах.", view),
                ("Добавить новый продукт в базу данных.", add),
                ("Удалить новый продукт из базы данных.", delete),
                ("Изменить данные о продукте в базе данных.", change),
                ("Записать таблицу в .txt фаил", wright_to_txt)
            };
            string description = "Вы зашли в систему хранилищя ресторана.\n" +
                "Ниже перечислены опции которые вы можете выбрать.\n\n" +
                 get_options(options_list) +
                 "\nВпишите номер варианта который хотите выбрать.";
            Console.Clear();
            Console.WriteLine(description);
            string input = Console.ReadLine();

            while (!check_list_input(input, options_list)) {
                input = Console.ReadLine();
            }
            options_list[Int32.Parse(input) - 1].action();
        }
        private void wright_to_txt() {
            Console.Clear();
            v.print_table_in_file();
            Notify?.Invoke("Данные успешно сохранились в фаил!\n" +
                "Чтобы вернуться нажмите Enter\n");
        }
        private void change()
        {
            Console.Clear();
            Console.WriteLine("Введите название продукта который хотите изменить: ");
            string input = Console.ReadLine();

            while (!check_has_product(input))
            {
                input = Console.ReadLine();
            }

            string current_name = input;
            Console.WriteLine("Данны о продукте в системе:\n");
            v.print_product(current_name);
            Console.WriteLine("Введите данные которые хотите изменить:\n");
            List<(string option, Action<string> action)> options_list = new List<(string option, Action<string> action)>() {
                ("Изменить имя.", change_name),
                ("Изменить систему измерения продукта.", change_count_type),
                ("Изменить количество.", change_count),
            };
            Console.WriteLine(get_options(options_list));
            input = Console.ReadLine();
            while (!check_list_input(input, options_list))
            {
                input = Console.ReadLine();
            }

            options_list[Int32.Parse(input) - 1].action(current_name);
        }
        private void change_name(string old_name) {
            Console.WriteLine("Введите новое имя для продукта\n" +
                "(1 слово, минимум 1 символ, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
            string input = Console.ReadLine();
            while (!check_unique_name(input) || !check_name_format(input))
            {
                input = Console.ReadLine();
            }
            m.change_name(old_name, input);
            Notify?.Invoke("Название было успешно изменено!\n" +
                "Чтобы вернуться нажмите Enter\n");
        }
        private void change_count_type(string old_name)
        {
            Console.WriteLine("Введите новую систему измерения продукта. \n" +
                "(1 слово, минимум 1 символ, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
            string input = Console.ReadLine();
            while (!check_name_format(input))
            {
                input = Console.ReadLine();
            }
            m.change_count_type(old_name, input);
            Notify?.Invoke("Система измерения продукта была успешно изменено!\n" +
                "Чтобы вернуться нажмите Enter\n");
        }
        private void change_count(string old_name){
            Console.WriteLine("Введите новое количество продукта. Можно в виде десятичного числа (через запятую!): ");
            string input = Console.ReadLine();
            while (!check_double_parse(input))
            {
                input = Console.ReadLine();
            }

            double count = -1;
            try
            {
                double tmp = double.Parse(input);
                count = tmp;
            }
            catch (FormatException e)
            {
                Console.WriteLine("Exception caught: {0}", e);
            }

            m.change_count(old_name, count);
            Notify?.Invoke("Количество было успешно изменено!\n" +
                "Чтобы вернуться нажмите Enter\n");
        }
        private void delete()
        {
            Console.Clear();
            Console.WriteLine("Ведите название продукта который хотите удалить: ");
            string input = Console.ReadLine();
            while (!check_has_product(input)) {
                input = Console.ReadLine();
            }
            m.remove_product(input);
            Notify?.Invoke("Продукт был успешно удален.\n" +
                "Чтобы вернуться нажмите Enter\n");
        }
        private void view(){
            Console.Clear();
            Console.WriteLine("Таблица данных о продуктах на складе:\n");
            v.print_table_in_console();
            Notify?.Invoke("Чтобы вернуться нажмите Enter\n");
        }
        private void add() {
            Console.Clear();
            Console.WriteLine("Чтобы добавить продукт введите данные о нем.\n");
            Console.WriteLine("Введите название продукта.\n" +
                "(1 слово, минимум 1 символ, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -):");
            string input = Console.ReadLine();

            while (!check_unique_name(input) || !check_name_format(input))
            {
                input = Console.ReadLine();
            }
            string name = input;


            Console.WriteLine("Введите систему измерения продукта.\n" +
                "(1 слово, минимум 1 символ, минимум 1 буква, можно использовать английские или русские буквы , цифры , знаки _ и -): ");
            input = Console.ReadLine();
            while (!check_name_format(input))
            {
                input = Console.ReadLine();
            }
            string count_type = input;
            

            Console.WriteLine("Введите количество продукта. Можно в виде десятичного числа (через запятую!): ");
            input = Console.ReadLine();
            while (!check_double_parse(input))
            {
                input = Console.ReadLine();
            }

            double count = -1;
            try
            {
                double tmp = double.Parse(input);
                count = tmp;
            }
            catch (FormatException e)
            {
                Console.WriteLine("Exception caught: {0}", e);
            }

            m.add_product(new Product(name, count_type, count));
            Notify?.Invoke("Продукт был успешно добавлен!\n" +
                "Чтобы вернуться нажмите Enter\n");
        }
        private bool check_empty_input(string input) {
            if (input == "")
            {
                Console.WriteLine("Название не может быть пустым.\nПопробуйте еще раз:");
                return true;
            }
            return false;
        }
        private bool check_name_format(string input) {  
            var hasChar = new Regex(@"[A-Za-zа-яА-Я]+");
            var valid = new Regex(@"^\s*[A-Za-zа-яА-Я0-9_-]{1,}\s*$");
            if (valid.IsMatch(input) && hasChar.IsMatch(input)) {
                return true;
            }
            Console.WriteLine("Имя не соответствует формату.\nПопробуйте еще раз:");
            return false;
        }
        private bool check_has_product(string input) {
            if (check_empty_input(input)) return false;

            if (m.has(input)) return true;
            else {
                Console.WriteLine("Такого продукта нету в системе!.\nПопробуйте еще раз:");
                return false;
            }
            
        }
        private bool check_unique_name(string input) {
            if (check_empty_input(input)) return false;
            
            else if (m.has(input))
            {
                Console.WriteLine("Такой елемент уже есть в системе.\nПопробуйте еще раз:");
                return false;
            }
            return true;
        }
        private bool check_double_parse(string input)
        {
            if (check_empty_input(input)) return false;
            if (!double.TryParse(input, out double i))
            {
                Console.WriteLine("Ваш ввод не соответствует формату десятичного числа!\nПопробуйте еще раз: ");
                return false;
            }
            if (i < 0)
            {
                Console.WriteLine("Число не может быть отрицательным!\nПопробуйте еще раз: ");
                return false;
            }
            return true;
        }

        private bool check_list_input<T>(string input, List<(string option, T action)> options_list) {
            if (!int.TryParse(input, out int i))
            {
                Console.WriteLine("Вы ввели не число!\nПопробуйте еще раз: ");
                return false;
            }
            else if (i < 1 || i > options_list.Count)
            {
                Console.WriteLine("Такого варианта нету в списке!\nПопробуйте еще раз: ");
                return false;
            }
            return true;
        }
    }

}
