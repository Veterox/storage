﻿using System;

namespace Storage
{
    [Serializable]
    class Product
    {
        public string name { get; set; }
        public string count_type { get; set; }
        public double count { get; set; }
        public Product(string name = "", string count_type = "", double count = 0) {
            this.name = name;
            this.count_type = count_type;
            this.count = count;
        }
        public static bool operator ==(Product p, string name)
        {
            return p.name == name;
        }
        public static bool operator !=(Product p, string name)
        {
            return p.name != name;
        }
    }
}
