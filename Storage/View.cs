﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Storage
{
    class View
    {
        Model m;
        public View(Model m)
        {
            this.m = m;
        }
        private string get_table_description() {
            List<Product> data = m.get_data();
            string description = "";

            double tmp = Math.Floor(Math.Log10(data.Count));
            int id_l = Convert.ToInt32((tmp + 1) > 0 ? tmp : 0);
            int m_n_l = m.get_max_name_length();
            int m_c_l = m.get_max_count_length();
            int m_сt_l = m.get_max_count_type_length();


            string id = String.Format("{0,-" + ((id_l > 1 ? id_l : 1) + 1) + "}", "#");
            string name = String.Format("{0,-" + ((m_n_l > 12 ? m_n_l : 12) + 2) + "}", "Наименование");
            string count = String.Format("{0,-" + ((m_c_l > 10 ? m_c_l : 10) + 2) + "}", "Количество");
            string count_type = String.Format("{0,-" + ((m_сt_l > 14 ? m_сt_l : 14) + 2) + "}", "Тип исчисления");

            description += " " + id + "|  " + name + "|  " + count + "|  " + count_type + "\n";
            int i = 1;
            foreach (Product p in data)
            {
                id = String.Format("{0,-" + ((id_l > 1 ? id_l : 1) + 1) + "}", i++);
                name = String.Format("{0,-" + ((m_n_l > 12 ? m_n_l : 12) + 2) + "}", p.name);
                count = String.Format("{0,-" + ((m_c_l > 10 ? m_c_l : 10) + 2) + "}", p.count);
                count_type = String.Format("{0,-" + ((m_сt_l > 14 ? m_сt_l : 14) + 2) + "}", p.count_type);
                description += " " + id + "|  " + name + "|  " + count + "|  " + count_type + "\n";
            }
            
            return description;
        }
        public void print_table_in_console()
        {
            Console.WriteLine(get_table_description());
        }
        public void print_table_in_file()
        {
            string filename = m.get_filename_txt();
            if (!File.Exists(filename))
            {
                try
                {
                    File.WriteAllLines(filename, new string[] { "" });
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\" + filename, false))
            {
                file.WriteLine(get_table_description());
            }
        }
        public void print_product(string prod_name)
        {
            Product prod = m.get_product(prod_name);
            int m_n_l = prod.name.Length;
            int m_сt_l = prod.count_type.Length;
            int m_c_l = prod.count.ToString().Length;
            string description = "";

            string name = String.Format("{0,-" + ((m_n_l > 12 ? m_n_l : 12) + 2) + "}", "Наименование");
            string count = String.Format("{0,-" + ((m_c_l > 10 ? m_c_l : 10) + 2) + "}", "Количество");
            string count_type = String.Format("{0,-" + ((m_сt_l > 14 ? m_сt_l : 14) + 2) + "}", "Тип исчисления");
            description += name + "|  " + count + "|  " + count_type + "\n";

            name = String.Format("{0,-" + ((m_n_l > 12 ? m_n_l : 12) + 2) + "}", prod.name);
            count = String.Format("{0,-" + ((m_c_l > 10 ? m_c_l : 10) + 2) + "}", prod.count);
            count_type = String.Format("{0,-" + ((m_сt_l > 14 ? m_сt_l : 14) + 2) + "}", prod.count_type);
            description += name + "|  " + count + "|  " + count_type + "\n";
            Console.WriteLine(description);
        }
    }
}
