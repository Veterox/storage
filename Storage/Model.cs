﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;

namespace Storage
{
    class Model
    {
        private List<Product> data = new List<Product>();
        private string filename_txt = "storage_data.txt";

        private string filename_dat = "data.dat";
        private BinaryFormatter formatter = new BinaryFormatter();

        static object locker = new object();

        public Model()
        {
            
            using (FileStream fs = new FileStream(filename_dat, FileMode.OpenOrCreate))
            {
                if (fs.Length != 0)
                    data = (List<Product>)formatter.Deserialize(fs);
            }
        }

        private void creat_thread_and_save_data() {
            Thread myThread = new Thread(save_data);
            myThread.Start();
        }

        private void save_data(){
            lock (locker)
            {
                using (FileStream fs = new FileStream(filename_dat, FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, data);
                }
            }
        }

        public void add_product(Product p) {
            data.Add(p);
            creat_thread_and_save_data();
        }
        public void change_name(string old_name, string new_name) {
            Product itemToChange = data.SingleOrDefault(r => r.name == old_name);
            if (itemToChange != null)
            {
                itemToChange.name = new_name;
                creat_thread_and_save_data();
            }
        }
        public void change_count_type(string name, string new_count_type)
        {
            Product itemToChange = data.SingleOrDefault(r => r.name == name);
            if (itemToChange != null)
            {
                itemToChange.count_type = new_count_type;
                creat_thread_and_save_data();
            }
        }
        public void change_count(string name, double new_count)
        {
            Product itemToChange = data.SingleOrDefault(r => r.name == name);
            if (itemToChange != null)
            {
                itemToChange.count = new_count;
                creat_thread_and_save_data();
            }
        }
        public void remove_product(string name)
        {
            Product itemToRemove = data.SingleOrDefault(r => r.name == name);
            if (itemToRemove != null)
            {
                data.Remove(itemToRemove);
                creat_thread_and_save_data();
            }
        }

        public bool has(string name)
        {
            return data.FindIndex(p => p == name) >= 0;
        }
        public List<Product> get_data() { 
            return data;
        }
        public string get_filename_txt() {
            return filename_txt;
        }
        public Product get_product(string name)
        {
            return  data.SingleOrDefault(r => r.name == name); ;
        }
        public int get_max_name_length() {
            if (data.Count != 0)
            {
                return data.Max(p => p.name.Length);
            }
            else {
                return 0;
            }
        }
        public int get_max_count_type_length()
        {
            if (data.Count != 0)
            {
                return data.Max(p => p.count_type.ToString().Length);
            }
            else
            {
                return 0;
            }
        }
        public int get_max_count_length()
        {
            if (data.Count != 0)
            {
                return data.Max(p => p.count.ToString().Length);
            }
            else
            {
                return 0;
            }
        }
    }
}
